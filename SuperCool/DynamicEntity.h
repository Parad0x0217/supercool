#ifndef DynamicEntity_h__
#define DynamicEntity_h__

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include "Entity.h"

class DynamicEntity : public virtual Entity
{
public:

	DynamicEntity(sf::RenderWindow* window) : DynamicEntity(window, sf::Vector2f(0, 0), sf::Vector2f(0, 0)) {}

	DynamicEntity(sf::RenderWindow* window, sf::Vector2f position) : DynamicEntity(window, position, sf::Vector2f(0, 0)) {}

	DynamicEntity(sf::RenderWindow* window, sf::Vector2f position, sf::Vector2f velocity) : Entity(window, position), _velocity(velocity) {}

	~DynamicEntity() {}

	virtual Command* handleInput() = 0;

	virtual void update() override
	{
		Command* command = handleInput();

		if (command)
		{
			command->execute();
		}

		_shape->move(_velocity);
	}

	sf::Vector2f getVelocity()
	{
		return _velocity;
	}

	void setVelocity(sf::Vector2f velocity)
	{
		_velocity = velocity;
	}

private:

	sf::Vector2f _velocity;

};

class ChangeVelocityCommand : public virtual Command
{
public:

	ChangeVelocityCommand(DynamicEntity* entity, sf::Vector2f velocity) : _entity(entity), _velocity(velocity) {}

	~ChangeVelocityCommand()
	{
		delete _entity;

		_entity = nullptr;
	}

	void execute()
	{
		_oldVelocity = _entity->getVelocity();
		_entity->setVelocity(_velocity);
	}

	void undo()
	{
		_entity->setVelocity(_oldVelocity);
	}

private:

	DynamicEntity* _entity = nullptr;

	sf::Vector2f _velocity;
	sf::Vector2f _oldVelocity;

};

#endif // DynamicEntity_h__
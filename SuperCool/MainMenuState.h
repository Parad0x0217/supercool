#ifndef MainMenuState_h__
#define MainMenuState_h__

#include "State.h"

class MainMenuState : public virtual State
{
public:

	MainMenuState(sf::RenderWindow* window);
	~MainMenuState();

	void handleEvents();

	void update();

	void draw();

private:

	std::vector<sf::Drawable*> _drawables;

	sf::Font* _font = nullptr;

	sf::Text* _playText = nullptr;
	sf::Text* _quitText = nullptr;

};
#endif // MainMenuState_h__
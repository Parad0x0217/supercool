#include "MainMenuState.h"

MainMenuState::MainMenuState(sf::RenderWindow* window) : State(window)
{
	_font = new sf::Font();

	if (!_font->loadFromFile("res/fonts/Roboto-Regular.ttf"))
		return;

	_playText = new sf::Text();
	_quitText = new sf::Text();

	_playText->setFont(*_font);
	_quitText->setFont(*_font);

	_playText->setString("Play!");
	_quitText->setString("Quit");

	_playText->setPosition(300, 300);
	_quitText->setPosition(200, 200);

	_drawables.push_back(_playText);
	_drawables.push_back(_quitText);
}

MainMenuState::~MainMenuState()
{
	delete _playText;
	delete _quitText;
}

void MainMenuState::handleEvents()
{
	sf::Event event;

	while (_window->pollEvent(event))
	{
		switch (event.type)
		{
			case sf::Event::Closed:
				_window->close();
				break;
			default:
				break;
		}
	}
}

void MainMenuState::update()
{
	
}

void MainMenuState::draw()
{
	for each (sf::Drawable* d in _drawables)
	{
		_window->draw(*d);
	}
}
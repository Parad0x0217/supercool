#ifndef StaticEntity_h__
#define StaticEntity_h__

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include "Entity.h"

class StaticEntity : public virtual Entity
{
public:

	StaticEntity(sf::RenderWindow* window) : StaticEntity(window, sf::Vector2f(0, 0)) {}

	StaticEntity(sf::RenderWindow* window, sf::Vector2f position) : Entity(window, position) {}

	~StaticEntity() {}

	virtual Command* handleInput() = 0;

	virtual void update() override
	{
		Command* command = handleInput();

		if (command)
		{
			command->execute();
		}
	}

private:

};
#endif // StaticEntity_h__
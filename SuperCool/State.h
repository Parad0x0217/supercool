#ifndef State_h__
#define State_h__

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

class State
{
public:

	State(sf::RenderWindow* window) : _window(window) {}
	~State() {}

	virtual void handleEvents() = 0;
	virtual void update() = 0;
	virtual void draw() = 0;

protected:

	sf::RenderWindow* _window = nullptr;

};
#endif // State_h__
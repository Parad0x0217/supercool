#ifndef PlayingState_h__
#define PlayingState_h__

#include <iostream>

#include "State.h"
#include "Entity.h"
#include "TestDynamicEntity.h"
#include "TestStaticEntity.h"

class PlayingState : public virtual State
{
public:

	PlayingState(sf::RenderWindow* window);
	~PlayingState();

	virtual void handleEvents() override;

	virtual void update() override;

	virtual void draw() override;

private:

	std::vector<Entity*> _entities;

};
#endif // PlayingState_h__
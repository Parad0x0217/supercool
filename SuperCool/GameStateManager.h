#ifndef GameStateManager_h__
#define GameStateManager_h__

#include <SFML/Window.hpp>

#include "State.h"

class GameStateManager
{
public:

	enum States
	{
		MainMenu,
		Playing,
		PauseMenu,
		Quit
	};

	GameStateManager(sf::RenderWindow* window);
	~GameStateManager();

	void setState(State* newState);
	State* getState();

	void handleEvents();
	void update();
	void draw();

private:

	State* _currentState = nullptr;

	sf::RenderWindow* _window = nullptr;

};
#endif // GameStateManager_h__
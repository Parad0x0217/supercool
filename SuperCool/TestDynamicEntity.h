#ifndef TestDynamicEntity_h__
#define TestDynamicEntity_h__

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include "DynamicEntity.h"

class TestDynamicEntity : public virtual DynamicEntity
{
public:

	TestDynamicEntity(sf::RenderWindow* window) : TestDynamicEntity(window, sf::Vector2f(0, 0)) {}

	TestDynamicEntity(sf::RenderWindow* window, sf::Vector2f position) : TestDynamicEntity(window, position, sf::Vector2f(0, 0)) {}

	TestDynamicEntity(sf::RenderWindow* window, sf::Vector2f position, sf::Vector2f velocity) : DynamicEntity(window, position, velocity), Entity(window, position) {}

	virtual Command* handleInput() override
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			return new MoveEntityCommand(this, sf::Vector2f(0, 0));

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			return new MoveEntityCommand(this, sf::Vector2f(400, 0));

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			return new MoveEntityCommand(this, sf::Vector2f(400, 300));

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			return new MoveEntityCommand(this, sf::Vector2f(500, 500));


		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			return new ChangeVelocityCommand(this, sf::Vector2f(0, -0.1f));

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			return new ChangeVelocityCommand(this, sf::Vector2f(-0.1f, 0));

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			return new ChangeVelocityCommand(this, sf::Vector2f(0, 0.1f));

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			return new ChangeVelocityCommand(this, sf::Vector2f(0.1f, 0));

		return nullptr;
	}

private:
	


};

#endif // TestDynamicEntity_h__

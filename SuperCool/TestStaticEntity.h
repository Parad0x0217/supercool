#ifndef TestStaticEntity_h__
#define TestStaticEntity_h__

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include "StaticEntity.h"

class TestStaticEntity : public virtual StaticEntity
{
public:

	TestStaticEntity(sf::RenderWindow* window) : TestStaticEntity(window, sf::Vector2f(0, 0)) {}

	TestStaticEntity(sf::RenderWindow* window, sf::Vector2f position) : StaticEntity(window, position), Entity(window, position) {}

	virtual Command* handleInput() override
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			return new MoveEntityCommand(this, sf::Vector2f(0, 0));

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			return new MoveEntityCommand(this, sf::Vector2f(400, 0));

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			return new MoveEntityCommand(this, sf::Vector2f(400, 300));

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			return new MoveEntityCommand(this, sf::Vector2f(500, 500));

		return nullptr;
	}

private:



};

#endif // TestStaticEntity_h__

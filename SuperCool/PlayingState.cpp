#include "PlayingState.h"

PlayingState::PlayingState(sf::RenderWindow* window) : State(window)
{
	TestDynamicEntity* tde = new TestDynamicEntity(window);
	tde->setVelocity(sf::Vector2f(0.1f, 0.1f));

	TestStaticEntity* tse = new TestStaticEntity(window);

	_entities.push_back(tde);
	_entities.push_back(tse);
}

PlayingState::~PlayingState()
{

}

void PlayingState::handleEvents()
{
	sf::Event event;

	while (_window->pollEvent(event))
	{
		switch (event.type)
		{
			case sf::Event::Closed:
				_window->close();
				break;
			default:
				break;
		}
	}
}

void PlayingState::update()
{
	for each (Entity* e in _entities)
	{
		e->update();
	}
}

void PlayingState::draw()
{
	for each (Entity* e in _entities)
	{
		e->draw();
	}
}
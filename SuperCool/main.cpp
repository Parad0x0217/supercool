#include <iostream>

#include <SFML/Main.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include "GameStateManager.h"

#include "MainMenuState.h"
#include "PlayingState.h"

int main()
{
	sf::RenderWindow* window = new sf::RenderWindow(sf::VideoMode(800, 600), "SuperCool", sf::Style::Titlebar | sf::Style::Close, sf::ContextSettings());

	GameStateManager* gsm = new GameStateManager(window);

	//gsm->setState(new MainMenuState(window));
	gsm->setState(new PlayingState(window));
	
	while (window->isOpen())
	{
		gsm->handleEvents();

		gsm->update();

		window->clear();
		
		gsm->draw();

		window->display();
	}

	delete gsm;
	delete window;

	return 0;
}
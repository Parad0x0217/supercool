#ifndef Entity_h__
#define Entity_h__

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include "Command.h"

class Entity
{
public:

	Entity(sf::RenderWindow* window) : Entity(window, sf::Vector2f(0, 0)) {}

	Entity(sf::RenderWindow* window, sf::Vector2f position) : _window(window), _position(position)
	{
		_shape = new sf::CircleShape(1.0f, 3);
		_shape->setFillColor(sf::Color::White);
		_shape->setRadius(10.0f);
		_shape->setPosition(_position);
	}

	~Entity()
	{
		delete _shape;
		delete _window;

		_shape = nullptr;
		_window = nullptr;
	}

	sf::Vector2f getPosition()
	{
		return _position;
	}

	void setPosition(sf::Vector2f position)
	{
		_position = position;
		_shape->setPosition(_position);
	}

	virtual Command* handleInput() = 0;
	virtual void update() = 0;
	
	void draw()
	{
		_window->draw(*_shape);
	}

protected:

	sf::CircleShape* _shape = nullptr;
	sf::Vector2f _position;
	sf::RenderWindow* _window = nullptr;

};

class MoveEntityCommand : public virtual Command
{
public:

	MoveEntityCommand(Entity* entity, sf::Vector2f position) : _entity(entity), _position(position) {}

	~MoveEntityCommand()
	{
		delete _entity;

		_entity = nullptr;
	}

	void execute()
	{
		_oldPosition = _entity->getPosition();
		_entity->setPosition(_position);
	}

	void undo()
	{
		_entity->setPosition(_oldPosition);
	}

private:

	Entity* _entity = nullptr;

	sf::Vector2f _position;
	sf::Vector2f _oldPosition;

};

#endif // Entity_h__
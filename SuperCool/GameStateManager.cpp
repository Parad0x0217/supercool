#include "GameStateManager.h"

GameStateManager::GameStateManager(sf::RenderWindow* window) : _window(window)
{
}

GameStateManager::~GameStateManager()
{
	
}

void GameStateManager::setState(State* newState)
{
	_currentState = newState;
}

State* GameStateManager::getState()
{
	return _currentState;
}

void GameStateManager::handleEvents()
{
	_currentState->handleEvents();
}

void GameStateManager::update()
{
	_currentState->update();
}

void GameStateManager::draw()
{
	_currentState->draw();
}

#ifndef Command_h__
#define Command_h__

class Command
{
public:

	virtual ~Command() {}

	virtual void execute() = 0;
	virtual void undo() = 0;
};

#endif // Command_h__
